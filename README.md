
# Reign Hacker News test

The server, once an hour, should connect to the API (refer to the below url) which shows
recently posted articles about Node.js on Hacker News. It should insert the data from the
API into a database and also define a REST API which the client (e.g. Postman) will be used
to retrieve the data.

Hacker News URL: https://hn.algolia.com/api/v1/search_by_date?query=nodejs
## Considerations

 - Node.js version active LTS
 - The server component should be coded in Javascript or TypeScript.
 - At least 30% test coverage (statements) for the server component.
 - The whole project has to be uploaded to GitLab.


## Stack
 - Active LTS version of Node.js + Express JS or Nest JS.
 - Database: MongoDB or PostgreSQL.
 - At least 30% test coverage (statements) for the server component.
 - ORM: Choose whatever you like.



## Run on Docker 🐳

Clone the project
```bash
  git clone git@gitlab.com:Monge1h/reign-hacker-news-api.git
```
* Create a .env file based on .env.example

Run docker!

```bash
  docker-compose up
```


## Run Locally

Clone the project

```bash
  git clone git@gitlab.com:Monge1h/reign-hacker-news-api.git
```

* Create a .env file based on .env.example

Install dependencies

```bash
  npm install
```

Run migrations to create tables and populate data

```bash
  npm run migrations:run
```

Start project

```bash
  npm run start:dev
```


## API Reference

Once the server starts running, you can go to: 
#### Get all items

```http
  http://localhost:3000/api/docs/
```
to see the documentation, also you have an exported postman file in order to test the API

Initial user credentials (this user is created with the initials migrations):
* email: admin@admin.com
* password: admin

## My approach

Based on the tech stack I decided to go with Nest Js and PostgreSQL because Nest Js
is a mature framework that has a lot of functionalities out of the box and I used a lot 
of them in the project.

The first thing that I worried about was that the server needs populate the data every hour,
but Nest js made this easy because already has a module that handles cron jobs!

To populate the data in the first init I use the migrations of Typeorm to create a seeder migration
with that migration I create a default User for the login and 20 items for the hacker news articles




## Lessons Learned

I learn a lot about different things doing this project, I learned about Docker,
 and how easy is to have your environment everywhere and I learned how the networks on Docker works.