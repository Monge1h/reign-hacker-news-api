import { Controller, Post, Body, UseGuards, Req } from '@nestjs/common';
import { Request } from 'express';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags } from '@nestjs/swagger';

import { LoginUserDto } from './dto/auth.dto';
import { User } from '../users/entities/user.entity';
import { AuthService } from './auth.service';
@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}
  @UseGuards(AuthGuard('local'))
  @Post('login')
  login(@Body() loginUserDto: LoginUserDto, @Req() req: Request) {
    const user = req.user as User;
    return this.authService.generateJwt(user);
  }
}