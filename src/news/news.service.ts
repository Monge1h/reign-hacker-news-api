import { HttpService } from '@nestjs/axios';
import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import { firstValueFrom } from 'rxjs';
import { map } from 'rxjs/operators';
import { Repository } from 'typeorm';
import { CreateNewsDto } from './news.dto';

import { News } from './news.entity';

@Injectable()
export class NewsService {
  constructor(
    @InjectRepository(News) private newsRepo: Repository<News>,
    private httpService: HttpService,
  ) {}

  findAll(params?: string, page?: number): Promise<News[]> {
    const builder = this.newsRepo.createQueryBuilder('news');
    console.log(params);
    if (params) {
      builder
        .where('news.active= :active', { active: 1 })
        .andWhere('news.author like :author', { author: `%${params}%` })
        .orWhere('news.title like :title', { title: `%${params}%` })
        .orWhere(`news._tags ::jsonb @> \'"${params}"\'`);
    }
    const currentPage: number = page || 1;
    const itemsPerPage = 5;

    builder.offset((currentPage - 1) * itemsPerPage).limit(itemsPerPage);
    return builder.getMany();
  }

  async delete(id: number){
     const news = await this.newsRepo.findOne(id);
    if (!news) {
      throw new NotFoundException('Item not found');
    } else {
      this.newsRepo.merge(news, { active: false });
      this.newsRepo.save(news)
      return {"msg":`news #${id} was deleted`};
    }
  }

  private async syncNews() {
    this.getNews().then(async (response) => {
      let newsData = response.hits;
      let oldNewsData = await this.newsRepo.find();
      // get just new news 🆕📰
      newsData = newsData.filter((news) => {
        return !oldNewsData.some(
          (oldNews) => oldNews.objectID == news.objectID,
        );
      });
      if (newsData.length == 0) {
        Logger.log('There is no new news ❌💾');
      } else {
        const dataToSave = newsData.map((item: CreateNewsDto) => {
          return {
            title: item.title,
            url: item.url,
            author: item.author,
            points: item.points,
            story_text: item.story_text,
            comment_text: item.comment_text,
            num_comments: item.num_comments,
            story_id: item.story_id,
            story_title: item.story_title,
            story_url: item.story_url,
            objectID: item.objectID,
            _tags: item._tags,
            _highlightResult: item._highlightResult,
            created_at: item.created_at,
          };
        });
        // 💾 save new items
        this.newsRepo.insert(dataToSave);
        Logger.log(`Inserted ${dataToSave.length} new items ✅💾`);
      }
    });
  }

  private getNews(): Promise<{ hits: any[] }> {
    return firstValueFrom(
      this.httpService
        .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
        .pipe(
          map((res) => {
            return res.data;
          }),
        ),
    );
  }
  @Cron("0 * * * *")
  handleCron(){
    this.syncNews()
  }
}
