import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { NewsController } from './news.controller';
import { NewsService } from './news.service';
import { News } from './news.entity';
import { HttpModule } from '@nestjs/axios';
@Module({
  imports: [TypeOrmModule.forFeature([News]), HttpModule],
  controllers: [NewsController],
  providers: [NewsService]
})
export class NewsModule {}
