import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class News {
  @PrimaryGeneratedColumn()
  id_news: number;

  @Column({ type: 'varchar', length: '255', nullable: true })
  title: string;

  @Column({ type: 'varchar', length: '255', nullable: true })
  url: string;

  @Column({ type: 'varchar', length: '255', nullable: true })
  author: string;

  @Column({ type: 'int', default: 0, nullable: true })
  points: number;

  @Column({ type: 'text', nullable: true })
  story_text: string;

  @Column({ type: 'text', nullable: true })
  comment_text: string;

  @Column({ type: 'int', default: 0, nullable: true })
  num_comments: number;

  @Column({ type: 'int', nullable: true })
  story_id: number;

  @Column({ type: 'varchar', length: '255', nullable: true })
  story_title: string;

  @Column({ type: 'varchar', length: '255', nullable: true })
  story_url: string;

  @Column({ type: 'int', nullable: true })
  objectID: number;

  @Column({ type: 'jsonb', nullable: true })
  _tags: string;

  @Column({ type: 'jsonb', nullable: true })
  _highlightResult: string;

  @Column({ type: 'timestamp', nullable: true })
  created_at: string;

  @Column({ type: 'boolean', nullable: true, default: true })
  active: boolean;
}