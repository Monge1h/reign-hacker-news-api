import {
  Controller,
  DefaultValuePipe,
  Get,
  ParseIntPipe,
  Query,
  Delete,
  Param,
  UseGuards,
} from '@nestjs/common';
import { ApiParam, ApiQuery, ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

import { NewsService } from './news.service';
import { News } from './news.entity';

@ApiTags('news')
@Controller('news')
export class NewsController {
  constructor(private newsService: NewsService) {}
  @ApiQuery({
    name: 'page',
    description: 'number of page that you want to see',
    type: 'number',
    required: false,
  })
  @ApiQuery({
    name: 'params',
    description: 'params to filter',
    type: 'string',
    required: false,
  })
  @Get()
  findAll(
    @Query('params') params = '',
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page = 1,
  ): Promise<News[]> {
    return this.newsService.findAll(params, page);
  }

  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth('Jwt')
  @ApiParam({
    name: 'id',
    description: 'id of news',
    type: 'number',
    required: true,
  })
  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.newsService.delete(id);
  }
}
