export class CreateNewsDto {
  readonly title: string;
  readonly url: string;
  readonly author: string;
  readonly points: number;
  readonly story_text: string;
  readonly comment_text: string;
  readonly num_comments: number;
  readonly story_id: number;
  readonly story_title: string;
  readonly story_url: string;
  readonly objectID: number;
  readonly _tags: string;
  readonly _highlightResult: string;
  readonly created_at: string;
  readonly active: boolean;
}
