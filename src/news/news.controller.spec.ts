import { Test, TestingModule } from '@nestjs/testing';
import { NewsController } from './news.controller';
import { NewsService } from './news.service';

describe('NewsController', () => {
  let controller: NewsController;
  let spyService: NewsService;

  beforeEach(async () => {
    const ApiServiceProvider = {
      provide: NewsService,
      useFactory: () =>({
        findAll: jest.fn(() => [])
      })
    }
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NewsController],
      providers: [ApiServiceProvider]
    }).compile();

    controller = module.get<NewsController>(NewsController);
    spyService = module.get<NewsService>(NewsService);
  });

  describe('Get news, findAll controller', () =>{
    it('should call findAll service from newsService',()=>{
      controller.findAll()
      expect(spyService.findAll).toHaveBeenCalled()
    })
  });
  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
