import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

import { CreateUserDto } from './dto/user.dto';
import { UpdateUserDto } from './dto/user.dto';
import { User } from './entities/user.entity';
@Injectable()
export class UsersService {
  constructor(@InjectRepository(User) private userRepo: Repository<User>) {}

  async create(createUserDto: CreateUserDto) {
    const newUser = this.userRepo.create(createUserDto);
    const hashPassword = await bcrypt.hash(newUser.password, 10);
    newUser.password = hashPassword;
    return this.userRepo.save(newUser);
  }

  findAll() {
    return this.userRepo.find();
  }

  async findOne(id: number) {
    const user = await this.userRepo.findOne(id);
    if (user) {
      return user;
    } else {
      throw new NotFoundException('User not found');
    }
  }

  async update(id: string, updateUserDto: UpdateUserDto) {
    const user = await this.userRepo.findOne(id);
    if (user) {
      this.userRepo.merge(user, updateUserDto);
      return this.userRepo.save(user);
    } else {
      throw new NotFoundException('User not found');
    }
  }

  async remove(id: number) {
    const user = await this.userRepo.findOne(id);
    if (user) {
      return this.userRepo.delete(id);
    } else {
      throw new NotFoundException('User not found');
    }
  }

  findByEmail(email: string) {
    return this.userRepo.findOne({ where: { email } });
  }
}
