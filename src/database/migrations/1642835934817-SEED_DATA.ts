import * as bcrypt from 'bcrypt';

import { NewsService } from "src/news/news.service";
import { User } from "src/users/entities/user.entity";
import { News } from "src/news/news.entity";

import {MigrationInterface, QueryRunner} from "typeorm";

export class SEEDDATA1642835934817 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
         // Populate data with News/articles! 📰📰
    let dataFromApi : {}[] = [
      {
        created_at: '2022-01-21T17:53:10.000Z',
        title: null,
        url: null,
        author: 'godelski',
        points: null,
        story_text: null,
        comment_text:
          'There\u0026#x27;s also risk to letting others host servers. We know about bad tor nodes. Federation doesn\u0026#x27;t solve the problem you\u0026#x27;re looking to solve',
        num_comments: null,
        story_id: 30013192,
        story_title: 'The most backdoor-looking bug I’ve ever seen (2021)',
        story_url: 'https://words.filippo.io/dispatches/telegram-ecdh/',
        parent_id: 30023310,
        created_at_i: 1642787590,
        _tags: ['comment', 'author_godelski', 'story_30013192'],
        objectID: '30027198',
        _highlightResult: {
          author: { value: 'godelski', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              "There's also risk to letting others host servers. We know about bad tor \u003cem\u003enodes\u003c/em\u003e. Federation doesn't solve the problem you're looking to solve",
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'The most backdoor-looking bug I’ve ever seen (2021)',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value: 'https://words.filippo.io/dispatches/telegram-ecdh/',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2022-01-21T17:21:35.000Z',
        title: null,
        url: null,
        author: 'thebean11',
        points: null,
        story_text: null,
        comment_text:
          'Do you mean people running the infrastructure? Or developing the software? We don\u0026#x27;t have to speculate on either. The infrastructure \u0026#x2F; nodes are paid for by some combination of inflation (block rewards) and transaction fees, depending on the network. The software is pretty similar to any other open source software. There are open source devs who will contribute for free, companies who will give grants for features they have a vested interest in (or will pay their own engineers to develop them directly), and organizations with funding to pay engineers.\u003cp\u003eAgain, we don\u0026#x27;t have to speculate. We don\u0026#x27;t even know who the founder of Bitcoin is and it\u0026#x27;s still under active development.\u003cp\u003eIn case you\u0026#x27;re interested, the mailing list is a great way to see what\u0026#x27;s happening in Bitcoin development: \u003ca href="https:\u0026#x2F;\u0026#x2F;lists.linuxfoundation.org\u0026#x2F;pipermail\u0026#x2F;bitcoin-dev\u0026#x2F;" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;lists.linuxfoundation.org\u0026#x2F;pipermail\u0026#x2F;bitcoin-dev\u0026#x2F;\u003c/a\u003e\u003cp\u003eFor Ethereum: \u003ca href="https:\u0026#x2F;\u0026#x2F;ethereum-magicians.org\u0026#x2F;" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;ethereum-magicians.org\u0026#x2F;\u003c/a\u003e',
        num_comments: null,
        story_id: 30025097,
        story_title:
          'Google Creates Blockchain Unit, Hires New ‘Founding Leader’',
        story_url:
          'https://blockworks.co/google-creates-blockchain-unit-hires-new-founding-leader/',
        parent_id: 30026551,
        created_at_i: 1642785695,
        _tags: ['comment', 'author_thebean11', 'story_30025097'],
        objectID: '30026650',
        _highlightResult: {
          author: { value: 'thebean11', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              'Do you mean people running the infrastructure? Or developing the software? We don\'t have to speculate on either. The infrastructure / \u003cem\u003enodes\u003c/em\u003e are paid for by some combination of inflation (block rewards) and transaction fees, depending on the network. The software is pretty similar to any other open source software. There are open source devs who will contribute for free, companies who will give grants for features they have a vested interest in (or will pay their own engineers to develop them directly), and organizations with funding to pay engineers.\u003cp\u003eAgain, we don\'t have to speculate. We don\'t even know who the founder of Bitcoin is and it\'s still under active development.\u003cp\u003eIn case you\'re interested, the mailing list is a great way to see what\'s happening in Bitcoin development: \u003ca href="https://lists.linuxfoundation.org/pipermail/bitcoin-dev/" rel="nofollow"\u003ehttps://lists.linuxfoundation.org/pipermail/bitcoin-dev/\u003c/a\u003e\u003cp\u003eFor Ethereum: \u003ca href="https://ethereum-magicians.org/" rel="nofollow"\u003ehttps://ethereum-magicians.org/\u003c/a\u003e',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value:
              'Google Creates Blockchain Unit, Hires New ‘Founding Leader’',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value:
              'https://blockworks.co/google-creates-blockchain-unit-hires-new-founding-leader/',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2022-01-21T17:16:45.000Z',
        title: null,
        url: null,
        author: 'tux3',
        points: null,
        story_text: null,
        comment_text:
          'This mandatory comment is your threadly reminder that node names are entirely unrelated to any physical dimensions, as has been the case for many years now.\u003cp\u003eYou should not interpret node names as meaning anything more than a point on the semiconductor roadmap. Nodes that share the same names across foundries will not have the same performance characteristics, but are usually roughly comparable (especially since Intel\u0026#x27;s new naming scheme).',
        num_comments: null,
        story_id: 30025695,
        story_title: 'Intel to invest up to $100 bln in Ohio chip plants',
        story_url:
          'https://www.reuters.com/technology/intel-plans-new-chip-manufacturing-site-ohio-report-2022-01-21/',
        parent_id: 30026373,
        created_at_i: 1642785405,
        _tags: ['comment', 'author_tux3', 'story_30025695'],
        objectID: '30026577',
        _highlightResult: {
          author: { value: 'tux3', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              "This mandatory comment is your threadly reminder that node names are entirely unrelated to any physical dimensions, as has been the case for many years now.\u003cp\u003eYou should not interpret node names as meaning anything more than a point on the semiconductor roadmap. \u003cem\u003eNodes\u003c/em\u003e that share the same names across foundries will not have the same performance characteristics, but are usually roughly comparable (especially since Intel's new naming scheme).",
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'Intel to invest up to $100 bln in Ohio chip plants',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value:
              'https://www.reuters.com/technology/intel-plans-new-chip-manufacturing-site-ohio-report-2022-01-21/',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2022-01-21T17:05:12.000Z',
        title: null,
        url: null,
        author: 'nobleach',
        points: null,
        story_text: null,
        comment_text:
          'Right, but if we\u0026#x27;re looking for the quickest path, \u0026quot;right click on this page, and select \u0026#x27;Inspect\u0026#x27;\u0026quot; and boom. You\u0026#x27;re there. Certainly one could go to \u003ca href="https:\u0026#x2F;\u0026#x2F;replit.com\u0026#x2F;" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;replit.com\u0026#x2F;\u003c/a\u003e and choose from a host of \u0026quot;better\u0026quot; languages. My approach was trying to avoid someone having to install anything at all. As soon as you say, \u0026quot;ok, you\u0026#x27;ll need to install NodeJS, then use npm to install this package....\u0026quot;, things get dicey. Someone who\u0026#x27;s serious about learning will fight through it, but someone who\u0026#x27;s just starting out may be put off.',
        num_comments: null,
        story_id: 30016323,
        story_title: 'SICP: JavaScript Edition available for pre-order',
        story_url:
          'https://mitpress.mit.edu/books/structure-and-interpretation-computer-programs-1',
        parent_id: 30017035,
        created_at_i: 1642784712,
        _tags: ['comment', 'author_nobleach', 'story_30016323'],
        objectID: '30026382',
        _highlightResult: {
          author: { value: 'nobleach', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              "Right, but if we're looking for the quickest path, \u0026quot;right click on this page, and select 'Inspect'\u0026quot; and boom. You're there. Certainly one could go to \u003ca href=\"https://replit.com/\" rel=\"nofollow\"\u003ehttps://replit.com/\u003c/a\u003e and choose from a host of \u0026quot;better\u0026quot; languages. My approach was trying to avoid someone having to install anything at all. As soon as you say, \u0026quot;ok, you'll need to install \u003cem\u003eNodeJS\u003c/em\u003e, then use npm to install this package....\u0026quot;, things get dicey. Someone who's serious about learning will fight through it, but someone who's just starting out may be put off.",
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'SICP: JavaScript Edition available for pre-order',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value:
              'https://mitpress.mit.edu/books/structure-and-interpretation-computer-programs-1',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2022-01-21T15:43:52.000Z',
        title: null,
        url: null,
        author: 'hosh',
        points: null,
        story_text: null,
        comment_text:
          'That\u0026#x27;s not a great comparison. The author probably should have compared Docker with Webassembly.\u003cp\u003eKubernetes isn\u0026#x27;t really about running many containers sharing a physical hardware. You can do that with Docker as well. What Kubernetes offers that Docker does not offer, is self-healing and resilience when managing many containers spread across many underlying nodes. There is no monolithic \u0026quot;Kubernetes\u0026quot; running. Rather, Kubernetes is designed as a set of microservices loosely coupled and maintaining desired states. The resiliency comes from that design.\u003cp\u003eBecause it is architected as microservices, Kubernetes is also extensible. Operators, for example, which manage a single distributed system (such as etcd, or postgres, or mongodb) can only be created because Kubernetes was architected as a set of loosely-coupled microservices.\u003cp\u003eWebAssembly is the container runtime without the orchestration capabilities. However, Kubernetes is capable of being modified such that it can schedule, orchestrate, and manage WebAssembly containers: \u003ca href="https:\u0026#x2F;\u0026#x2F;cloudblogs.microsoft.com\u0026#x2F;opensource\u0026#x2F;2020\u0026#x2F;04\u0026#x2F;07\u0026#x2F;announcing-krustlet-kubernetes-rust-kubelet-webassembly-wasm\u0026#x2F;" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;cloudblogs.microsoft.com\u0026#x2F;opensource\u0026#x2F;2020\u0026#x2F;04\u0026#x2F;07\u0026#x2F;annou...\u003c/a\u003e',
        num_comments: null,
        story_id: 30020121,
        story_title: 'WebAssembly: The New Kubernetes?',
        story_url:
          'https://wingolog.org/archives/2021/12/13/webassembly-the-new-kubernetes',
        parent_id: 30020121,
        created_at_i: 1642779832,
        _tags: ['comment', 'author_hosh', 'story_30020121'],
        objectID: '30025079',
        _highlightResult: {
          author: { value: 'hosh', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              'That\'s not a great comparison. The author probably should have compared Docker with Webassembly.\u003cp\u003eKubernetes isn\'t really about running many containers sharing a physical hardware. You can do that with Docker as well. What Kubernetes offers that Docker does not offer, is self-healing and resilience when managing many containers spread across many underlying \u003cem\u003enodes\u003c/em\u003e. There is no monolithic \u0026quot;Kubernetes\u0026quot; running. Rather, Kubernetes is designed as a set of microservices loosely coupled and maintaining desired states. The resiliency comes from that design.\u003cp\u003eBecause it is architected as microservices, Kubernetes is also extensible. Operators, for example, which manage a single distributed system (such as etcd, or postgres, or mongodb) can only be created because Kubernetes was architected as a set of loosely-coupled microservices.\u003cp\u003eWebAssembly is the container runtime without the orchestration capabilities. However, Kubernetes is capable of being modified such that it can schedule, orchestrate, and manage WebAssembly containers: \u003ca href="https://cloudblogs.microsoft.com/opensource/2020/04/07/announcing-krustlet-kubernetes-rust-kubelet-webassembly-wasm/" rel="nofollow"\u003ehttps://cloudblogs.microsoft.com/opensource/2020/04/07/annou...\u003c/a\u003e',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'WebAssembly: The New Kubernetes?',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value:
              'https://wingolog.org/archives/2021/12/13/webassembly-the-new-kubernetes',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2022-01-21T15:14:18.000Z',
        title: null,
        url: null,
        author: 'matthewaveryusa',
        points: null,
        story_text: null,
        comment_text:
          'This is why rust is so exciting: it\u0026#x27;s the first new language that\u0026#x27;s graduated from toy-language space we\u0026#x27;ve seen in a while without a runtime. (python, ruby, go and typescript-nodejs are the other graduates I\u0026#x27;m thinking about.)',
        num_comments: null,
        story_id: 30022022,
        story_title: 'ISO C became unusable for operating systems development',
        story_url: 'https://arxiv.org/abs/2201.07845',
        parent_id: 30024295,
        created_at_i: 1642778058,
        _tags: ['comment', 'author_matthewaveryusa', 'story_30022022'],
        objectID: '30024568',
        _highlightResult: {
          author: {
            value: 'matthewaveryusa',
            matchLevel: 'none',
            matchedWords: [],
          },
          comment_text: {
            value:
              "This is why rust is so exciting: it's the first new language that's graduated from toy-language space we've seen in a while without a runtime. (python, ruby, go and typescript-\u003cem\u003enodejs\u003c/em\u003e are the other graduates I'm thinking about.)",
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'ISO C became unusable for operating systems development',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value: 'https://arxiv.org/abs/2201.07845',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2022-01-21T13:13:55.000Z',
        title: null,
        url: null,
        author: 'cjbgkagh',
        points: null,
        story_text: null,
        comment_text:
          'They start off by being very bad at search, they then compound the issue by creating the Library concept that transparency blends multiple nodes in the tree structure together. Then they make it needlessly slow.',
        num_comments: null,
        story_id: 30018763,
        story_title: 'The Windows 11 taskbar is an annoying step backward',
        story_url:
          'https://www.pcworld.com/article/549576/the-windows-11-taskbar-is-an-annoying-step-backward.html',
        parent_id: 30020396,
        created_at_i: 1642770835,
        _tags: ['comment', 'author_cjbgkagh', 'story_30018763'],
        objectID: '30023006',
        _highlightResult: {
          author: { value: 'cjbgkagh', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              'They start off by being very bad at search, they then compound the issue by creating the Library concept that transparency blends multiple \u003cem\u003enodes\u003c/em\u003e in the tree structure together. Then they make it needlessly slow.',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'The Windows 11 taskbar is an annoying step backward',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value:
              'https://www.pcworld.com/article/549576/the-windows-11-taskbar-is-an-annoying-step-backward.html',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2022-01-21T13:05:07.000Z',
        title: null,
        url: null,
        author: 'me_me_me',
        points: null,
        story_text: null,
        comment_text:
          'What?? Noooo... they would only have to launch indefinitely something like 10 rockets a month to keep replacing aging nodes, I am sure that is financially viable way to compete with a copper\u0026#x2F;fibre cable -_-\u003cp\u003eThe tech works, its just completely non feasible. Geosync satelites can give you global access with handful of satelites.\u003cp\u003eTrans-sea cables give you cheap and quick connection speeds.\u003cp\u003eStarlink is middle of the road solution that solves both problems are immense costs.\u003cp\u003eI can only imagine high frequency traders being able to afford it vs utilise the advantage of the latencies.',
        num_comments: null,
        story_id: 30022304,
        story_title:
          'Starlink satellites hindering detection of near-Earth asteroids, study finds',
        story_url:
          'https://web.archive.org/web/20220120172234/https://www.dailystar.co.uk/news/latest-news/elon-musks-starlink-satellites-hindering-26001740',
        parent_id: 30022708,
        created_at_i: 1642770307,
        _tags: ['comment', 'author_me_me_me', 'story_30022304'],
        objectID: '30022901',
        _highlightResult: {
          author: { value: 'me_me_me', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              'What?? Noooo... they would only have to launch indefinitely something like 10 rockets a month to keep replacing aging \u003cem\u003enodes\u003c/em\u003e, I am sure that is financially viable way to compete with a copper/fibre cable -_-\u003cp\u003eThe tech works, its just completely non feasible. Geosync satelites can give you global access with handful of satelites.\u003cp\u003eTrans-sea cables give you cheap and quick connection speeds.\u003cp\u003eStarlink is middle of the road solution that solves both problems are immense costs.\u003cp\u003eI can only imagine high frequency traders being able to afford it vs utilise the advantage of the latencies.',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value:
              'Starlink satellites hindering detection of near-Earth asteroids, study finds',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value:
              'https://web.archive.org/web/20220120172234/https://www.dailystar.co.uk/news/latest-news/elon-musks-starlink-satellites-hindering-26001740',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2022-01-21T01:30:51.000Z',
        title: 'Tuning HTTP Keep-Alive to Prevent Gateway Errors',
        url: 'https://connectreport.com/blog/tuning-http-keep-alive-in-node-js/',
        author: 'lemax',
        points: 2,
        story_text: null,
        comment_text: null,
        num_comments: 0,
        story_id: null,
        story_title: null,
        story_url: null,
        parent_id: null,
        created_at_i: 1642728651,
        _tags: ['story', 'author_lemax', 'story_30018014'],
        objectID: '30018014',
        _highlightResult: {
          title: {
            value: 'Tuning HTTP Keep-Alive to Prevent Gateway Errors',
            matchLevel: 'none',
            matchedWords: [],
          },
          url: {
            value:
              'https://connectreport.com/blog/tuning-http-keep-alive-in-\u003cem\u003enode-js\u003c/em\u003e/',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          author: { value: 'lemax', matchLevel: 'none', matchedWords: [] },
        },
      },
      {
        created_at: '2022-01-20T11:21:15.000Z',
        title: null,
        url: null,
        author: '0x008',
        points: null,
        story_text: null,
        comment_text:
          'Is binary size a concern to you (probably not if you come from go)? If not: don\u0026#x27;t worry about it. You can compile everything into a binary when you embed the runtime. Ever thought about JVM-languages?\u003cp\u003eDo you need static typing, multithreading and fast execution? Maybe try Scala or Clojure.\u003cp\u003eIf functional programming is not your main concern, but you want a mature ecosystem with one-liner http server and medium-fast execution? Maybe try python or nodejs.',
        num_comments: null,
        story_id: 29971945,
        story_title:
          'Ask HN: Is there a functional programming language with the benefits of Go?',
        story_url: null,
        parent_id: 29971945,
        created_at_i: 1642677675,
        _tags: ['comment', 'author_0x008', 'story_29971945'],
        objectID: '30007481',
        _highlightResult: {
          author: { value: '0x008', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              "Is binary size a concern to you (probably not if you come from go)? If not: don't worry about it. You can compile everything into a binary when you embed the runtime. Ever thought about JVM-languages?\u003cp\u003eDo you need static typing, multithreading and fast execution? Maybe try Scala or Clojure.\u003cp\u003eIf functional programming is not your main concern, but you want a mature ecosystem with one-liner http server and medium-fast execution? Maybe try python or \u003cem\u003enodejs\u003c/em\u003e.",
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value:
              'Ask HN: Is there a functional programming language with the benefits of Go?',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2022-01-20T05:36:01.000Z',
        title: null,
        url: null,
        author: 'chrischen',
        points: null,
        story_text: null,
        comment_text:
          'All the companies using Rescript (formerly ReasonML + Bucklescript compiler) and ones using Reasonml right now are effectively using Ocaml. It allows you to run Ocaml but target web browsers or Nodejs.\u003cp\u003eThis includes apps like TinyMCE, ahrefs, and around 50% of messenger.com code [1].\u003cp\u003e[1] \u003ca href="https:\u0026#x2F;\u0026#x2F;reasonml.github.io\u0026#x2F;blog\u0026#x2F;2017\u0026#x2F;09\u0026#x2F;08\u0026#x2F;messenger-50-reason" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;reasonml.github.io\u0026#x2F;blog\u0026#x2F;2017\u0026#x2F;09\u0026#x2F;08\u0026#x2F;messenger-50-reas...\u003c/a\u003e',
        num_comments: null,
        story_id: 29998957,
        story_title:
          'Hotcaml: An OCaml interpreter with watching and reloading',
        story_url: 'https://github.com/let-def/hotcaml',
        parent_id: 30001688,
        created_at_i: 1642656961,
        _tags: ['comment', 'author_chrischen', 'story_29998957'],
        objectID: '30005126',
        _highlightResult: {
          author: { value: 'chrischen', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              'All the companies using Rescript (formerly ReasonML + Bucklescript compiler) and ones using Reasonml right now are effectively using Ocaml. It allows you to run Ocaml but target web browsers or \u003cem\u003eNodejs\u003c/em\u003e.\u003cp\u003eThis includes apps like TinyMCE, ahrefs, and around 50% of messenger.com code [1].\u003cp\u003e[1] \u003ca href="https://reasonml.github.io/blog/2017/09/08/messenger-50-reason" rel="nofollow"\u003ehttps://reasonml.github.io/blog/2017/09/08/messenger-50-reas...\u003c/a\u003e',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'Hotcaml: An OCaml interpreter with watching and reloading',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value: 'https://github.com/let-def/hotcaml',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2022-01-19T18:25:38.000Z',
        title: null,
        url: null,
        author: 'grumblepeet',
        points: null,
        story_text: null,
        comment_text:
          'Can definitely recommend Tiddlywiki. Ive just spent several weeks implementing a mini ZettelKasten using Tiddlywiki. It is created in \u0026#x27;exploded\u0026#x27; mode in NodeJS and then a script runs to re-assemble it into a single html file (with the images in a separate folder) and then I push it to GitHub where it is published on to a personal domain. It is visible here: \u003ca href="https:\u0026#x2F;\u0026#x2F;chloetiddlykasten.chloegilbert.me" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;chloetiddlykasten.chloegilbert.me\u003c/a\u003e\u003cp\u003eI used the template (TZK) created by Soren Bjornstad to build it.',
        num_comments: null,
        story_id: 29996037,
        story_title: 'A tour to my Zettelkasten note clusters',
        story_url:
          'https://lmy.medium.com/a-tour-to-my-zettelkasten-notes-dc26a75e5257',
        parent_id: 29997618,
        created_at_i: 1642616738,
        _tags: ['comment', 'author_grumblepeet', 'story_29996037'],
        objectID: '29997801',
        _highlightResult: {
          author: {
            value: 'grumblepeet',
            matchLevel: 'none',
            matchedWords: [],
          },
          comment_text: {
            value:
              'Can definitely recommend Tiddlywiki. Ive just spent several weeks implementing a mini ZettelKasten using Tiddlywiki. It is created in \'exploded\' mode in \u003cem\u003eNodeJS\u003c/em\u003e and then a script runs to re-assemble it into a single html file (with the images in a separate folder) and then I push it to GitHub where it is published on to a personal domain. It is visible here: \u003ca href="https://chloetiddlykasten.chloegilbert.me" rel="nofollow"\u003ehttps://chloetiddlykasten.chloegilbert.me\u003c/a\u003e\u003cp\u003eI used the template (TZK) created by Soren Bjornstad to build it.',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'A tour to my Zettelkasten note clusters',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value:
              'https://lmy.medium.com/a-tour-to-my-zettelkasten-notes-dc26a75e5257',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2022-01-19T17:55:49.000Z',
        title: null,
        url: null,
        author: 'matthall28',
        points: null,
        story_text: null,
        comment_text:
          'A tiny API for embedding weather forecasts as an image: \u003ca href="https:\u0026#x2F;\u0026#x2F;weatherembed.com\u0026#x2F;" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;weatherembed.com\u0026#x2F;\u003c/a\u003e\u003cp\u003eMakes around $500\u0026#x2F;month from various subscriptions through RapidAPI. Built on a whim during the pandemic. Uses Google Cloud Run + NodeJS.',
        num_comments: null,
        story_id: 29995152,
        story_title:
          'Ask HN: Those making $500/month on side projects in 2022 – Show and tell',
        story_url: null,
        parent_id: 29995152,
        created_at_i: 1642614949,
        _tags: ['comment', 'author_matthall28', 'story_29995152'],
        objectID: '29997282',
        _highlightResult: {
          author: { value: 'matthall28', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              'A tiny API for embedding weather forecasts as an image: \u003ca href="https://weatherembed.com/" rel="nofollow"\u003ehttps://weatherembed.com/\u003c/a\u003e\u003cp\u003eMakes around $500/month from various subscriptions through RapidAPI. Built on a whim during the pandemic. Uses Google Cloud Run + \u003cem\u003eNodeJS\u003c/em\u003e.',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value:
              'Ask HN: Those making $500/month on side projects in 2022 – Show and tell',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2022-01-19T12:49:27.000Z',
        title: 'How to Set Up a Node.js Project with TypeScript',
        url: 'https://blog.appsignal.com/2022/01/19/how-to-set-up-a-nodejs-project-with-typescript.html',
        author: 'unripe_syntax',
        points: 1,
        story_text: null,
        comment_text: null,
        num_comments: 0,
        story_id: null,
        story_title: null,
        story_url: null,
        parent_id: null,
        created_at_i: 1642596567,
        _tags: ['story', 'author_unripe_syntax', 'story_29992912'],
        objectID: '29992912',
        _highlightResult: {
          title: {
            value: 'How to Set Up a Node.js Project with TypeScript',
            matchLevel: 'none',
            matchedWords: [],
          },
          url: {
            value:
              'https://blog.appsignal.com/2022/01/19/how-to-set-up-a-\u003cem\u003enodejs\u003c/em\u003e-project-with-typescript.html',
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          author: {
            value: 'unripe_syntax',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2022-01-19T08:35:39.000Z',
        title: null,
        url: null,
        author: 'dreamsbythelake',
        points: null,
        story_text: null,
        comment_text:
          'What a coincidence! Lovely topic, even registered account for this :-)\u003cp\u003eI _just_finished_ my own comparative benchmarks to (re)check my projects from ~7 years ago, all in similar stack.\u003cp\u003eBack then I wrote the logic as Apache modules, in C. It was using Cairo to draw charts (surprisingly, the traces of trigonometry knowledge was enough for me to code that :-), and I had absolutely crazy \u0026quot;hybrids\u0026quot; of bubble charts with bars,    alpha channel overlays etc. It was extremely useful for my projects back then and I never seen any library, able to produce what I \u0026quot;tailored\u0026quot; ...)\u003cp\u003eThe 7-years-ago end-to-end page generation time was ~300 mcs (1e-6 sec), with graphics, data store IO and request processing, preparing the \u0026quot;bucket brigade\u0026quot; and passing it down the Apache chain.\u003cp\u003eThis Jan I re-visited my code and implemented logic for OpenBSD httpd as:\u003cp\u003e** 1) Open BSD httpd \u0026quot;patch\u0026quot; to hijack the request processing internally, do necessary data and graph ops and push the result into Bufferevent buffer directly, before httpd serves it up to the client.\u003cp\u003e** 2) FCGI responder app, talking to httpd over unix socket. \u003ci\u003eBTW\u003c/i\u003e: this is \u003ci\u003emost\u003c/i\u003e secure version I know of, I could chroot \u0026#x2F; pledge \u0026#x2F; unveil and, IMO, it beats SELinux and anything else.\u003cp\u003e3) CGI script in ksh\u0026lt;=\u0026gt;slowcgi\u0026lt;=\u0026gt;FCGI=\u0026gt;httpd\u003cp\u003e4) CGI program (statically linked) in pure C\u0026lt;=\u0026gt;slowcgi\u0026lt;=\u0026gt;FCGI=\u0026gt;httpd\u003cp\u003e5) PHP :-) page (no frameworks)\u0026lt;=\u0026gt;php-fpm (with OpCache)\u0026lt;=\u0026gt;FCGI=\u0026gt;httpd\u003cp\u003eTo my extreme surprise, the outcome was clear - \u003ci\u003eit did not matter\u003c/i\u003e what I wrote my logic in, _anything today_ (including CGI shell script) is so fast, that 90% of time was spent on Network communication between the WebServer and the Browser. (And with TLS it is like 2x penalty ...)\u003cp\u003eAll options above gave me end-to-end page generation time about 1-1.5 ms.\u003cp\u003eGuess what? Beyond \u0026quot;Hello World\u0026quot;, with page size of 500Kb+, PHP was faster than anything else, including native \u0026quot;httpd patch\u0026quot; in C.\u003cp\u003eAs side effect, I also confirmed that Libevent-based absolutely gorgeous OpenBSD httpd works slightly slower than standard pre-fork Apache httpd from pkg_add. (It gave me sub-ms times, just like 7 years ago)\u003cp\u003eWho would say ...\u003cp\u003eWhat also happened is that \u003ci\u003eany\u003c/i\u003e framework (PHP or I even tried nodejs) or writing CGI in Python increased my end-to-end page generation time 10x, to double-digit ms.\u003cp\u003eI remember last week someone here was talking about writing business applications \u0026#x2F; servers for clients in C++, delivering them as single executable file.\u003cp\u003eI would be very interested to hear how that person\u0026#x27;s observations correlate with mine above.\u003cp\u003eG\u0026#x27;day everyone!',
        num_comments: null,
        story_id: 29988951,
        story_title: 'BCHS: OpenBSD, C, httpd and SQLite web stack',
        story_url: 'https://learnbchs.org/index.html',
        parent_id: 29988951,
        created_at_i: 1642581339,
        _tags: ['comment', 'author_dreamsbythelake', 'story_29988951'],
        objectID: '29991248',
        _highlightResult: {
          author: {
            value: 'dreamsbythelake',
            matchLevel: 'none',
            matchedWords: [],
          },
          comment_text: {
            value:
              "What a coincidence! Lovely topic, even registered account for this :-)\u003cp\u003eI _just_finished_ my own comparative benchmarks to (re)check my projects from ~7 years ago, all in similar stack.\u003cp\u003eBack then I wrote the logic as Apache modules, in C. It was using Cairo to draw charts (surprisingly, the traces of trigonometry knowledge was enough for me to code that :-), and I had absolutely crazy \u0026quot;hybrids\u0026quot; of bubble charts with bars,    alpha channel overlays etc. It was extremely useful for my projects back then and I never seen any library, able to produce what I \u0026quot;tailored\u0026quot; ...)\u003cp\u003eThe 7-years-ago end-to-end page generation time was ~300 mcs (1e-6 sec), with graphics, data store IO and request processing, preparing the \u0026quot;bucket brigade\u0026quot; and passing it down the Apache chain.\u003cp\u003eThis Jan I re-visited my code and implemented logic for OpenBSD httpd as:\u003cp\u003e** 1) Open BSD httpd \u0026quot;patch\u0026quot; to hijack the request processing internally, do necessary data and graph ops and push the result into Bufferevent buffer directly, before httpd serves it up to the client.\u003cp\u003e** 2) FCGI responder app, talking to httpd over unix socket. \u003ci\u003eBTW\u003c/i\u003e: this is \u003ci\u003emost\u003c/i\u003e secure version I know of, I could chroot / pledge / unveil and, IMO, it beats SELinux and anything else.\u003cp\u003e3) CGI script in ksh\u0026lt;=\u0026gt;slowcgi\u0026lt;=\u0026gt;FCGI=\u0026gt;httpd\u003cp\u003e4) CGI program (statically linked) in pure C\u0026lt;=\u0026gt;slowcgi\u0026lt;=\u0026gt;FCGI=\u0026gt;httpd\u003cp\u003e5) PHP :-) page (no frameworks)\u0026lt;=\u0026gt;php-fpm (with OpCache)\u0026lt;=\u0026gt;FCGI=\u0026gt;httpd\u003cp\u003eTo my extreme surprise, the outcome was clear - \u003ci\u003eit did not matter\u003c/i\u003e what I wrote my logic in, _anything today_ (including CGI shell script) is so fast, that 90% of time was spent on Network communication between the WebServer and the Browser. (And with TLS it is like 2x penalty ...)\u003cp\u003eAll options above gave me end-to-end page generation time about 1-1.5 ms.\u003cp\u003eGuess what? Beyond \u0026quot;Hello World\u0026quot;, with page size of 500Kb+, PHP was faster than anything else, including native \u0026quot;httpd patch\u0026quot; in C.\u003cp\u003eAs side effect, I also confirmed that Libevent-based absolutely gorgeous OpenBSD httpd works slightly slower than standard pre-fork Apache httpd from pkg_add. (It gave me sub-ms times, just like 7 years ago)\u003cp\u003eWho would say ...\u003cp\u003eWhat also happened is that \u003ci\u003eany\u003c/i\u003e framework (PHP or I even tried \u003cem\u003enodejs\u003c/em\u003e) or writing CGI in Python increased my end-to-end page generation time 10x, to double-digit ms.\u003cp\u003eI remember last week someone here was talking about writing business applications / servers for clients in C++, delivering them as single executable file.\u003cp\u003eI would be very interested to hear how that person's observations correlate with mine above.\u003cp\u003eG'day everyone!",
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'BCHS: OpenBSD, C, httpd and SQLite web stack',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value: 'https://learnbchs.org/index.html',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2022-01-19T07:50:23.000Z',
        title: null,
        url: null,
        author: 'saberdancer',
        points: null,
        story_text: null,
        comment_text:
          'Given your experience, it should be extremely simple, especially if all you want to do is drop NginX infront of your NodeJS server. All you have to do is install it and add a line in the config that points traffic to your NodeJs instance. Btw often times people use Nginx as SSL termination and use http to the backend (if on the same instance).\u003cp\u003eLet\u0026#x27;s Encrypt can automatically add lines to the nginx config that enable SSL, but in some cases it doesn\u0026#x27;t work properly and the config is malformed. In any case not hard to fix.',
        num_comments: null,
        story_id: 29985871,
        story_title: 'Do svidaniya, Igor, and thank you for Nginx',
        story_url:
          'https://www.nginx.com/blog/do-svidaniya-igor-thank-you-for-nginx/',
        parent_id: 29990007,
        created_at_i: 1642578623,
        _tags: ['comment', 'author_saberdancer', 'story_29985871'],
        objectID: '29991020',
        _highlightResult: {
          author: {
            value: 'saberdancer',
            matchLevel: 'none',
            matchedWords: [],
          },
          comment_text: {
            value:
              "Given your experience, it should be extremely simple, especially if all you want to do is drop NginX infront of your \u003cem\u003eNodeJS\u003c/em\u003e server. All you have to do is install it and add a line in the config that points traffic to your \u003cem\u003eNodeJs\u003c/em\u003e instance. Btw often times people use Nginx as SSL termination and use http to the backend (if on the same instance).\u003cp\u003eLet's Encrypt can automatically add lines to the nginx config that enable SSL, but in some cases it doesn't work properly and the config is malformed. In any case not hard to fix.",
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'Do svidaniya, Igor, and thank you for Nginx',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value:
              'https://www.nginx.com/blog/do-svidaniya-igor-thank-you-for-nginx/',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2022-01-19T06:26:08.000Z',
        title: null,
        url: null,
        author: 'fdfsdfasdfd',
        points: null,
        story_text: null,
        comment_text:
          'I\u0026#x27;m just a fool on the internet, but if your appserver is NodeJS, you might want to consider HAProxy over nginx (I say this as a fan of nginx).\u003cp\u003eThe reason being that (unless my information is stale), NodeJS will happily accept all the connections thrown at it, eventually causing each connection to be starved of compute capacity and finally falling over.  HAProxy is able to keep a connection queue and feed a maximum of (for example) 4 concurrent requests to the backend(s), thus providing back-pressure to incoming requests.  Makes it a lot easier if you need to eventually scale your app horizontally, too.',
        num_comments: null,
        story_id: 29985871,
        story_title: 'Do svidaniya, Igor, and thank you for Nginx',
        story_url:
          'https://www.nginx.com/blog/do-svidaniya-igor-thank-you-for-nginx/',
        parent_id: 29990007,
        created_at_i: 1642573568,
        _tags: ['comment', 'author_fdfsdfasdfd', 'story_29985871'],
        objectID: '29990624',
        _highlightResult: {
          author: {
            value: 'fdfsdfasdfd',
            matchLevel: 'none',
            matchedWords: [],
          },
          comment_text: {
            value:
              "I'm just a fool on the internet, but if your appserver is \u003cem\u003eNodeJS\u003c/em\u003e, you might want to consider HAProxy over nginx (I say this as a fan of nginx).\u003cp\u003eThe reason being that (unless my information is stale), \u003cem\u003eNodeJS\u003c/em\u003e will happily accept all the connections thrown at it, eventually causing each connection to be starved of compute capacity and finally falling over.  HAProxy is able to keep a connection queue and feed a maximum of (for example) 4 concurrent requests to the backend(s), thus providing back-pressure to incoming requests.  Makes it a lot easier if you need to eventually scale your app horizontally, too.",
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'Do svidaniya, Igor, and thank you for Nginx',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value:
              'https://www.nginx.com/blog/do-svidaniya-igor-thank-you-for-nginx/',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2022-01-19T04:34:26.000Z',
        title: null,
        url: null,
        author: 'graderjs',
        points: null,
        story_text: null,
        comment_text:
          'I\u0026#x27;m an experienced dev with a deep knowledge of full stack web as well as infrastructure but I\u0026#x27;ve never put nginx in front of any app. I want to but I honestly avoided it because I thought it was difficult. This may not be the right place to ask but is there a good guide for someone who\u0026#x27;s deep into nodejs who just wants to set up nginx on Debian in front of a node server with https? and just see how it goes.',
        num_comments: null,
        story_id: 29985871,
        story_title: 'Do svidaniya, Igor, and thank you for Nginx',
        story_url:
          'https://www.nginx.com/blog/do-svidaniya-igor-thank-you-for-nginx/',
        parent_id: 29985871,
        created_at_i: 1642566866,
        _tags: ['comment', 'author_graderjs', 'story_29985871'],
        objectID: '29990007',
        _highlightResult: {
          author: { value: 'graderjs', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              "I'm an experienced dev with a deep knowledge of full stack web as well as infrastructure but I've never put nginx in front of any app. I want to but I honestly avoided it because I thought it was difficult. This may not be the right place to ask but is there a good guide for someone who's deep into \u003cem\u003enodejs\u003c/em\u003e who just wants to set up nginx on Debian in front of a node server with https? and just see how it goes.",
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'Do svidaniya, Igor, and thank you for Nginx',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value:
              'https://www.nginx.com/blog/do-svidaniya-igor-thank-you-for-nginx/',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
      {
        created_at: '2022-01-19T01:43:37.000Z',
        title: 'Show HN: NFT Options Trading on Solana',
        url: null,
        author: 'the_black_hand',
        points: 3,
        story_text:
          'GM to all,\u003cp\u003eWe\u0026#x27;ve been working on an opensource NPM package and tool https:\u0026#x2F;\u0026#x2F;nftoptions.app for creating options contracts on the Solana blockchain. The client is in nodejs https:\u0026#x2F;\u0026#x2F;www.npmjs.com\u0026#x2F;package\u0026#x2F;solana-options , with an onchain server written in Rust https:\u0026#x2F;\u0026#x2F;github.com\u0026#x2F;dbanda\u0026#x2F;solana-nft-options\u003cp\u003eThis package allows you to easily create options contracts for tokens on Solana, with specific expiry times and strike values on the blockchain in one line of code. Similarly, you can use it to mint tokens and create contracts on those tokens. This allows you to do things like mint an NFT for a piece of art and then sell a call contract to buy that NFT at a predefined price. Each contract is tied to an NFT too, so you can easily trade them as well.\u003cp\u003eThe onchain program is currently available on Solana devnet and testnet. We hope to launch on mainnet soon and possibly drop the NFTs on platforms such as Solsea for easy exchange. Currently, the web app https:\u0026#x2F;\u0026#x2F;nftoptions.app only supports the Phantom wallet but we are looking to add more soon.\u003cp\u003eWe are looking for feedback, contributors and testers. Issues can be filed on the GitHub. As always, any feedback is greatly appreciated.\u003cp\u003eThanks y’all.',
        comment_text: null,
        num_comments: 0,
        story_id: null,
        story_title: null,
        story_url: null,
        parent_id: null,
        created_at_i: 1642556617,
        _tags: ['story', 'author_the_black_hand', 'story_29988799', 'show_hn'],
        objectID: '29988799',
        _highlightResult: {
          title: {
            value: 'Show HN: NFT Options Trading on Solana',
            matchLevel: 'none',
            matchedWords: [],
          },
          author: {
            value: 'the_black_hand',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_text: {
            value:
              "GM to all,\u003cp\u003eWe've been working on an opensource NPM package and tool https://nftoptions.app for creating options contracts on the Solana blockchain. The client is in \u003cem\u003enodejs\u003c/em\u003e https://www.npmjs.com/package/solana-options , with an onchain server written in Rust https://github.com/dbanda/solana-nft-options\u003cp\u003eThis package allows you to easily create options contracts for tokens on Solana, with specific expiry times and strike values on the blockchain in one line of code. Similarly, you can use it to mint tokens and create contracts on those tokens. This allows you to do things like mint an NFT for a piece of art and then sell a call contract to buy that NFT at a predefined price. Each contract is tied to an NFT too, so you can easily trade them as well.\u003cp\u003eThe onchain program is currently available on Solana devnet and testnet. We hope to launch on mainnet soon and possibly drop the NFTs on platforms such as Solsea for easy exchange. Currently, the web app https://nftoptions.app only supports the Phantom wallet but we are looking to add more soon.\u003cp\u003eWe are looking for feedback, contributors and testers. Issues can be filed on the GitHub. As always, any feedback is greatly appreciated.\u003cp\u003eThanks y’all.",
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
        },
      },
      {
        created_at: '2022-01-18T19:53:21.000Z',
        title: null,
        url: null,
        author: 'smashah',
        points: null,
        story_text: null,
        comment_text:
          'Hi HN,\u003cp\u003eI\u0026#x27;m pumped to launch payitfwd.dev - an easy way for github project maintainers to redirect would-be donors to downstream dependencies.\u003cp\u003eI built payitfwd over the weekend after reading the n-th post on HN about maintainer burnout (specifically about adhole).\u003cp\u003eAt the moment, I\u0026#x27;m a full-time open-source maintainer and thankfully don\u0026#x27;t fully rely on donations. Despite that I tried to come up with novel ways to incentivise donations until I realised donations aren\u0026#x27;t meant to be transactional and thought \u0026quot;Ok, if they\u0026#x27;re not going to donate to me, they should at least donate to the many dependencies the project uses and therefore they rely on.\u0026quot; and payitfwd.dev was born.\u003cp\u003epayitfwd uses GH\u0026#x27;s graphql API to query a project\u0026#x27;s dependency graph for dependency funding links. It then simply redirects the browser to a random dependency\u0026#x27;s funding link.\u003cp\u003eWhether you\u0026#x27;re a big tech co with a spare spot on your SDK repo or a maintainer that wants to give visibility to the people that make your own project possible - as long as you have a GH dependency graph, payitfwd.dev will work for you.\u003cp\u003eHow does it work?\u003cp\u003eIf you want to create a payitfwd link for your project, simply replace \u0026quot;github.com\u0026quot; with \u0026quot;payitfwd.dev\u0026quot; on your repo url. Just a heads up, it takes a while the first time.\u003cp\u003ee.g: \u003ca href="https:\u0026#x2F;\u0026#x2F;payitfwd.dev\u0026#x2F;open-wa\u0026#x2F;wa-automate-nodejs" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;payitfwd.dev\u0026#x2F;open-wa\u0026#x2F;wa-automate-nodejs\u003c/a\u003e\u003cp\u003eWho is this for?\u003cp\u003eIf you have a spare spot in your projects\u0026#x27; list of funding links and you\u0026#x2F;your org don\u0026#x27;t already need all possible donations yourself then this is for you.\u003cp\u003eDo I have to replace all my funding links with payitfwd?\u003cp\u003eNo you absolutely do not have to do that. You don\u0026#x27;t have to use it at all.\u003cp\u003eWhy would I forward donators to someone else if I already have a hard time getting donators myself?\u003cp\u003eI see this in the same light as a secret santa, or paying for the car behind you in a drive through. payitfwd.dev v allows people to exercise their unique blend of altruism in an indirect and discrete manner.',
        num_comments: null,
        story_id: 29984743,
        story_title: 'Show HN: Payitfwd.dev – Redirect Donors to Dependencies',
        story_url: 'https://payitfwd.dev/',
        parent_id: 29984743,
        created_at_i: 1642535601,
        _tags: ['comment', 'author_smashah', 'story_29984743'],
        objectID: '29984768',
        _highlightResult: {
          author: { value: 'smashah', matchLevel: 'none', matchedWords: [] },
          comment_text: {
            value:
              "Hi HN,\u003cp\u003eI'm pumped to launch payitfwd.dev - an easy way for github project maintainers to redirect would-be donors to downstream dependencies.\u003cp\u003eI built payitfwd over the weekend after reading the n-th post on HN about maintainer burnout (specifically about adhole).\u003cp\u003eAt the moment, I'm a full-time open-source maintainer and thankfully don't fully rely on donations. Despite that I tried to come up with novel ways to incentivise donations until I realised donations aren't meant to be transactional and thought \u0026quot;Ok, if they're not going to donate to me, they should at least donate to the many dependencies the project uses and therefore they rely on.\u0026quot; and payitfwd.dev was born.\u003cp\u003epayitfwd uses GH's graphql API to query a project's dependency graph for dependency funding links. It then simply redirects the browser to a random dependency's funding link.\u003cp\u003eWhether you're a big tech co with a spare spot on your SDK repo or a maintainer that wants to give visibility to the people that make your own project possible - as long as you have a GH dependency graph, payitfwd.dev will work for you.\u003cp\u003eHow does it work?\u003cp\u003eIf you want to create a payitfwd link for your project, simply replace \u0026quot;github.com\u0026quot; with \u0026quot;payitfwd.dev\u0026quot; on your repo url. Just a heads up, it takes a while the first time.\u003cp\u003ee.g: \u003ca href=\"https://payitfwd.dev/open-wa/wa-automate-nodejs\" rel=\"nofollow\"\u003ehttps://payitfwd.dev/open-wa/wa-automate-\u003cem\u003enodejs\u003c/em\u003e\u003c/a\u003e\u003cp\u003eWho is this for?\u003cp\u003eIf you have a spare spot in your projects' list of funding links and you/your org don't already need all possible donations yourself then this is for you.\u003cp\u003eDo I have to replace all my funding links with payitfwd?\u003cp\u003eNo you absolutely do not have to do that. You don't have to use it at all.\u003cp\u003eWhy would I forward donators to someone else if I already have a hard time getting donators myself?\u003cp\u003eI see this in the same light as a secret santa, or paying for the car behind you in a drive through. payitfwd.dev v allows people to exercise their unique blend of altruism in an indirect and discrete manner.",
            matchLevel: 'full',
            fullyHighlighted: false,
            matchedWords: ['nodejs'],
          },
          story_title: {
            value: 'Show HN: Payitfwd.dev – Redirect Donors to Dependencies',
            matchLevel: 'none',
            matchedWords: [],
          },
          story_url: {
            value: 'https://payitfwd.dev/',
            matchLevel: 'none',
            matchedWords: [],
          },
        },
      },
    ];

     const dataToSave = dataFromApi.map((item: any) => {
       return {
         title: item.title,
         url: item.url,
         author: item.author,
         points: item.points,
         story_text: item.story_text,
         comment_text: item.comment_text,
         num_comments: item.num_comments,
         story_id: item.story_id,
         story_title: item.story_title,
         story_url: item.story_url,
         objectID: item.objectID,
         _tags: item._tags,
         _highlightResult: item._highlightResult,
         created_at: item.created_at,
       };
     });
     // 💾 save new items
     await queryRunner.manager.insert(News,dataToSave)
     // Create new user 😀
     const hashPassword = await bcrypt.hash('admin', 10);
     await queryRunner.manager.save(User, {
       email: 'admin@admin.com',
       password: hashPassword,
     });
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
