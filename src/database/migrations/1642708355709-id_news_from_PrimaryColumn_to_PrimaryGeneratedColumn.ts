import {MigrationInterface, QueryRunner} from "typeorm";

export class idNewsFromPrimaryColumnToPrimaryGeneratedColumn1642708355709 implements MigrationInterface {
    name = 'idNewsFromPrimaryColumnToPrimaryGeneratedColumn1642708355709'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE SEQUENCE IF NOT EXISTS "news_id_news_seq" OWNED BY "news"."id_news"`);
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "id_news" SET DEFAULT nextval('"news_id_news_seq"')`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "id_news" DROP DEFAULT`);
        await queryRunner.query(`DROP SEQUENCE "news_id_news_seq"`);
    }

}
