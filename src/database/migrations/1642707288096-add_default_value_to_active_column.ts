import {MigrationInterface, QueryRunner} from "typeorm";

export class addDefaultValueToActiveColumn1642707288096 implements MigrationInterface {
    name = 'addDefaultValueToActiveColumn1642707288096'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "active" SET DEFAULT true`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "news" ALTER COLUMN "active" DROP DEFAULT`);
    }

}
