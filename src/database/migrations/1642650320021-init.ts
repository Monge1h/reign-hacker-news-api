import {MigrationInterface, QueryRunner} from "typeorm";

export class init1642650320021 implements MigrationInterface {
    name = 'init1642650320021'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "news" ("id_news" integer NOT NULL, "title" character varying(255), "url" character varying(255), "author" character varying(255), "points" integer DEFAULT '0', "story_text" text, "comment_text" text, "num_comments" integer DEFAULT '0', "story_id" integer, "story_title" character varying(255), "story_url" character varying(255), "objectId" integer, "_tags" jsonb, "_highlightResult" jsonb, "created_at" TIMESTAMP, "active" boolean, CONSTRAINT "PK_7b45d20b149146c3ee47c08b689" PRIMARY KEY ("id_news"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "news"`);
    }

}
