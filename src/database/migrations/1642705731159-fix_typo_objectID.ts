import {MigrationInterface, QueryRunner} from "typeorm";

export class fixTypoObjectID1642705731159 implements MigrationInterface {
    name = 'fixTypoObjectID1642705731159'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "news" RENAME COLUMN "objectId" TO "objectID"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "news" RENAME COLUMN "objectID" TO "objectId"`);
    }

}
